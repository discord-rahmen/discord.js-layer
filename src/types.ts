import type { PresenceData, ClientOptions, Client } from "discord.js";

export interface DRClientOptions {
    token: string;
    precense: PresenceData;
    clientOptions: ClientOptions;
}

export interface DRClientClass {
    wrapperClient: Client;
}

export type DRCommandOptionType = "STRING"
                                | "NUMBER"
                                | "BOOLEAN"
                                | "SUB_COMMAND"
                                | "SUB_COMMAND_GROUP"
                                | "USER"
                                | "CHANNEL"
                                | "ROLE"
                                | "MENTIONABLE"
                                | "INTEGER"
                                | "ATTACHMENT";

export type DRCommandType = "CHAT_INPUT" | "USER" | "MESSAGE";